﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using AsynchronousTest.Library.Standard;

namespace AsynchronousTest.Asp.Net.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly Service _service = new Service();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetRandomNumber(bool configureAwait, bool wait)
        {
            int threadBefore = Thread.CurrentThread.ManagedThreadId;

            Response response;
            if (wait)
            {
                response = _service.OperationAsync(configureAwait).Result;
            }
            else
            {
                response = await _service.OperationAsync(configureAwait);
            }

            int threadAfter = Thread.CurrentThread.ManagedThreadId;

            return Json(new { threadBefore, response, threadAfter }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ImplicitParallelism(bool configureAwait)
        {
            var tasks = new[]
            {
                _service.VoidOperationAsync(configureAwait),
                _service.VoidOperationAsync(configureAwait)
            };

            int threadBefore = Thread.CurrentThread.ManagedThreadId;
            await Task.WhenAll(tasks);
            int threadAfter = Thread.CurrentThread.ManagedThreadId;

            return Json(new { threadBefore, result = _service.Results, threadAfter }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> TestContext(bool configureAwait)
        {
            System.Web.HttpContext.Current.Response.StatusCode = 200;
            int threadBefore = Thread.CurrentThread.ManagedThreadId;
            await Task.Run(() =>
            {
                Thread.Sleep(1000);
            }).ConfigureAwait(configureAwait);

            int threadAfter = Thread.CurrentThread.ManagedThreadId;
            System.Web.HttpContext.Current.Response.StatusCode = 404;

            return Json(new { threadBefore, threadAfter }, JsonRequestBehavior.AllowGet);
        }
    }
}