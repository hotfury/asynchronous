﻿namespace AsynchronousTest.Library.Standard
{
    public class Response
    {
        public int RandomNumber { get; set; }

        public int RandomNumberThreadId { get; set; }

        public int ResponseAfterThreadId { get; set; }

        public int? RandomNumberSyncContextHash { get; set; }

        public int? CurrentSyncContextHash { get; set; }
    }
}