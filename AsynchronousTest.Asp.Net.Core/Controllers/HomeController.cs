﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AsynchronousTest.Library.Standard;
using Microsoft.AspNetCore.Mvc;

namespace AsynchronousTest.Asp.Net.Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly Service _service = new Service();

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetRandomNumber(bool configureAwait, bool wait)
        {
            int threadBefore = Thread.CurrentThread.ManagedThreadId;

            Response response;
            if (wait)
            {
                response = _service.OperationAsync(configureAwait).Result; 
            }
            else
            {
                response = await _service.OperationAsync(configureAwait);
            }

            int threadAfter = Thread.CurrentThread.ManagedThreadId;

            return Json(new { threadBefore, response, threadAfter });
        }

        public async Task<IActionResult> ImplicitParallelism(bool configureAwait)
        {
            int threadBefore = Thread.CurrentThread.ManagedThreadId;
            
            var tasks = new[]
            {
                _service.VoidOperationAsync(configureAwait),
                _service.VoidOperationAsync(configureAwait)
            };
            
            await Task.WhenAll(tasks);
            int threadAfter = Thread.CurrentThread.ManagedThreadId;

            return Json(new { threadBefore, result = _service.Results, threadAfter });
        }
    }
}