﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AsynchronousTest.Library.Standard
{
    public class Service
    {
        private const int SleepTime = 1000;

        public List<Response> Results { get; } = new List<Response>();

        public async Task<Response> OperationAsync(bool configureAwait)
        {
            var result = await Task.Run(() =>
            {
                Thread.Sleep(SleepTime);
                
                return new Response
                {
                    RandomNumber = new Random().Next(),
                    RandomNumberThreadId = Thread.CurrentThread.ManagedThreadId,
                };

            }).ConfigureAwait(configureAwait);

            return result;
        }

        public async Task VoidOperationAsync(bool configureAwait)
        {
            Response result = await OperationAsync(configureAwait)
                .ConfigureAwait(configureAwait);
            result.ResponseAfterThreadId = Thread.CurrentThread.ManagedThreadId;
            
            Results.Add(result); //Thread unsafe for ASP.NET Core but safe for ASP.NET MVC
        }
    }
}